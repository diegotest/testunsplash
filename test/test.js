import React, { View, Text, StyleSheet } from 'react-native';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import ListPhotos from '../app/components/list.photos';

const Test = React.createClass({
  render() {
    return (
      <View>
        <ListPhotos photos={[{ id: '1', width: 300, height: 300, urls: { small: 'https://upload.wikimedia.org/wikipedia/commons/8/84/Example.svg' } }]} />
      </View>
    )
  }
});
describe('<Test />', () => {
  it('it should render 1 view component', () => {
    const wrapper = shallow(<Test />);
    expect(wrapper.find(View)).to.have.length(1);
  });
});

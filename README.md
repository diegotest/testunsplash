# README #

This README would normally document whatever steps are necessary to get your application up and running.

### How install this? ###

* download the project
* run in command line: "npm install"
* run in command line: "react-native android-run"

### Test ###

* run in command line: "npm test"

### Downlod Apk ###

* you can download the apk from: "https://drive.google.com/file/d/1qO3hCwoOkEu-C3uQndMjdk2PZCcmrWHn/view?usp=sharing"

### Generate Apk ###

* run in command line: "react-native bundle --dev false --platform android --entry-file index.js --bundle-output ./android/app/build/intermediates/assets/debug/index.android.bundle --assets-dest ./android/app/build/intermediates/res/merged/debug"
* run in command line: "cd android && gradlew assembleDebug"
* next you can find the apk in: "android/app/build/outputs/apk/"


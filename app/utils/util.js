import Dimensions from 'Dimensions';
import { Platform, StyleSheet } from 'react-native';
import Unsplash from 'unsplash-js/native';

export const myWidth = Dimensions.get('window').width;
export const myHeight = Dimensions.get('window').height;
export const platformDiff = (Platform.OS === 'ios') ? 20 : 0;

export const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
    padding: 10,
    textAlign: 'center'
  },
  activityIndicatorContainer: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  }
});

export const indicator = {
  size: 80,
  borderWidth: 1,
  color: '#F5FCFF',
  unfilledColor: 'rgba(200, 200, 200, 0.8)'
};

export const unsplash = new Unsplash({
  applicationId: '0f12f19f17819a1bcd378ea8a2e5f36b91f57c0f32f802cbb23e12a4d5c67181',
  secret: '354a4393b4b3e14e7361d7924621b071e9250f85c61cb4436e09b173148003e7',
  callbackUrl: 'urn:ietf:wg:oauth:2.0:oob'
});

export const authenticationUrl = unsplash.auth.getAuthenticationUrl([
  'public',
  'write_collections'
]);

export const idCollection = 1929402;

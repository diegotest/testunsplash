import { combineReducers } from 'redux';

import { ADD_PHOTO, GET_PHOTOS, SEARCH_PHOTOS } from '../actions/unsplash.actions'

let dataState = { photos: [], photosSearch: [], loading: true, photo: {} };

const dataReducer = (state = dataState, action) => {
  switch (action.type) {
    case GET_PHOTOS:
      return { ...state, photos: action.photos, loading: false };
    case SEARCH_PHOTOS:
      return { ...state, photosSearch: action.photosSearch, loading: false };
    case ADD_PHOTO:
      // const newState = Object.assign([], state);
      // newState.photosSearch = newState.photosSearch.filter((item, index) => (item.id != action.photo.id))
      // return newState;
      return { ...state, photos: [action.photos, ...state.photos], photosSearch: state.photosSearch.filter((item, index) => (item.id !== action.photo.id)), loading: false };
    default:
      return state;
  }
};

// Combine all the reducers
const rootReducer = combineReducers({
  dataReducer
})

export default rootReducer;

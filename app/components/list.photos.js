import React, { Component } from 'react';
import {
  View,
  FlatList
} from 'react-native';

import Image from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Bar';

import { myWidth, myHeight, indicator } from '../utils/util';

export default class ListPhotos extends Component {
  constructor(props) {
    super(props);
    this.renderItem = this.renderItem.bind(this);
  }

  render() {
    return (
      <View>
        <FlatList
          data={this.props.photos}
          renderItem={this.renderItem}
          keyExtractor={(item, index) => index} />
      </View>
    );
  }

  renderItem({ item, index }) {
    let scale = 0;
    if (myHeight < myWidth) {
      scale = myHeight / item.height;
    } else {
      scale = myWidth / item.width;
    }

    var styleItem = {
      width: item.width * scale,
      height: item.height * scale
    }

    return (
      <Image
        source={{ uri: item != null ? item.urls.small : null }}
        indicator={ProgressBar}
        indicatorProps={indicator}
        style={item === null || item === undefined ? {} : styleItem}
      />
    )
  }
}

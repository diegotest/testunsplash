import { toJson } from 'unsplash-js/native';
import { unsplash, idCollection } from '../utils/util';

export const GET_PHOTOS = 'GET_PHOTOS';
export const SEARCH_PHOTOS = 'SEARCH_PHOTOS';
export const ADD_PHOTO = 'ADD_PHOTO';

export function getPhotos() {
  return (dispatch) => {
    unsplash.collections.getCollectionPhotos(idCollection, 1, 100, 'latest')
      .then(toJson)
      .then(json => {
        dispatch({ type: GET_PHOTOS, photos: json });
      });
  };
}

export function searchPhotos(text) {
  return (dispatch) => {
    unsplash.search.photos(text, 1)
      .then(toJson)
      .then(json => {
        dispatch({ type: SEARCH_PHOTOS, photosSearch: json.results });
      });
  };
}

export function addPhoto(photo) {
  return (dispatch) => {
    unsplash.collections.addPhotoToCollection(idCollection, photo.id)
      .then(toJson)
      .then(json => {
        dispatch({ type: ADD_PHOTO, photos: photo, photo: photo });
      });
  };
}

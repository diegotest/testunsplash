import React, { Component } from 'react';
import {
  View,
  Button,
  Text,
  FlatList,
  ActivityIndicator
} from 'react-native';

import Image from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Bar';
import { styles } from '../utils/util';
import ListPhotos from '../components/list.photos'

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../actions/unsplash.actions';

class Home extends Component {

  static navigationOptions = {
    title: 'Home',
  };


  constructor(props) {
    super(props);
    this.state = {
    }
  }


  componentDidMount() {
    this.props.getPhotos();
  }

  render() {
    const { navigate } = this.props.navigation;

    if (this.props.loading) {
      return (
        <View style={styles.activityIndicatorContainer}>
          <ActivityIndicator animating={true} />
        </View>
      );
    }

    return (

      <View style={styles.container}>
        <Button
          title="Add Photo"
          onPress={() =>
            navigate('AddPhoto')
          }
        />

        <Text style={styles.titleText}>
          My Collection
        </Text>

        <ListPhotos photos={this.props.photos} />

      </View>

    );
  }
}

// The function takes data from the app current state,
// and insert/links it into the props of our component.
// This function makes Redux know that this component needs to be passed a piece of the state
function mapStateToProps(state, props) {
  return {
    loading: state.dataReducer.loading,
    photos: state.dataReducer.photos
  }
}

// Doing this merges our actions into the component’s props,
// while wrapping them in dispatch() so that they immediately dispatch an Action.
// Just by doing this, we will have access to the actions defined in out actions file (action/home.js)
function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

//Connect everything
export default connect(mapStateToProps, mapDispatchToProps)(Home);


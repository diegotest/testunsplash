import React, { Component } from 'react';

import {
  View,
  Button,
  TextInput
} from 'react-native';

import { myWidth, myHeight, platformDiff, styles, unsplash, idCollection } from '../utils/util';
import { toJson } from 'unsplash-js/native';
import ListPhotosAdd from '../components/list.photos.add'

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../actions/unsplash.actions';

class AddPhoto extends Component {

  static navigationOptions = {
    title: 'Add Photo',
  };

  constructor(props) {
    super(props);
    this.state = {
      text: "dogs"
    }
    this.addPhotoToCollection = this.addPhotoToCollection.bind(this);
    this.searchPhotos = this.searchPhotos.bind(this);
  }

  searchPhotos() {
    this.props.searchPhotos(this.state.text);
  }

  addPhotoToCollection(photo) {
    //console.log(photo.id);
    this.props.addPhoto(photo);
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>

        <TextInput
          style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
          onChangeText={(text) => this.setState({ text })}
          value={this.state.text}
        />

        <Button
          title="Search"
          onPress={this.searchPhotos.bind(this)}
        />

        <ListPhotosAdd photos={this.props.photosSearch} addPhotoToCollection={this.addPhotoToCollection} />

      </View>
    );
  }
}


// The function takes data from the app current state,
// and insert/links it into the props of our component.
// This function makes Redux know that this component needs to be passed a piece of the state
function mapStateToProps(state, props) {
  return {
    loading: state.dataReducer.loading,
    photosSearch: state.dataReducer.photosSearch
  }
}

// Doing this merges our actions into the component’s props,
// while wrapping them in dispatch() so that they immediately dispatch an Action.
// Just by doing this, we will have access to the actions defined in out actions file (action/home.js)
function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

//Connect everything
export default connect(mapStateToProps, mapDispatchToProps)(AddPhoto);


import { AppRegistry } from 'react-native';

import React, { Component } from 'react'

import {
  StackNavigator
} from 'react-navigation';

import AddPhoto from './app/pages/addphoto';
import Home from './app/pages/home';

import configureStore from './app/store';
import { Provider } from 'react-redux'
const store = configureStore()

const App = StackNavigator({
  Home: { screen: Home },
  AddPhoto: { screen: AddPhoto }
});

export class MyCounterApp extends Component {
  render() {
    return (
      // <Provider> allows us to access the store for dispatching actions and receiving data from
      // the state in it's children i.e. <App/>
      <Provider store={store}>
        <App />
      </Provider>
    )
  }
}

AppRegistry.registerComponent('testUnsplash', () => MyCounterApp);
